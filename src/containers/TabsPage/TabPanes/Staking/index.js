import React, { useState } from 'react';
import MnemonicModal from '_components/MnemonicModal';
import { Button } from 'antd';
import axios from 'axios';
import { FormattedMessage } from '@/utils/locale';
import { postPromise, message } from '@/utils/helper';

let type = '';

const Staking = () => {
  const [mnemonicModalVisible, setMnemonicModalVisible] = useState(false);
  const [stakingText, setStakingText] = useState(localStorage.isStaking || 'false');

  const changeStaking = (tp) => {
    type = tp;
    setMnemonicModalVisible(true);
  };

  return (
    <div className="staking">
      <h3><FormattedMessage id="staking info" /></h3>
      <table>
        <tbody>
          <tr>
            <td><FormattedMessage id="is staking" /></td>
            <td className="is_staking">{stakingText}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="start/stop/refresh" /></td>
            <td>
              <Button id="refresh_staking" onClick={() => changeStaking('isstaking')}><FormattedMessage id="refresh" /></Button>
              <Button id="stop_staking" onClick={() => changeStaking('stopstaking')}><FormattedMessage id="stop staking" /></Button>
              <Button id="start_staking" onClick={() => changeStaking('startstaking')}><FormattedMessage id="start staking" /></Button>
            </td>
          </tr>
        </tbody>
      </table>
      <MnemonicModal
        visible={mnemonicModalVisible}
        onCancel={() => setMnemonicModalVisible(false)}
        onVerifySuccess={(mnemonic) => {
          const url = `/${type}`;
          const postdata = {
            mnemonic,
          };

          postPromise(url, postdata).then(async (ret) => {
            const msg = ret === 'false' || !ret ? 'FAILED!' : 'SUCCESS!';
            if (ret === 'false' || !ret) {
              message.warning(`${type} ${msg}`);
            } else {
              message.success(`${type} ${msg}`);
            }

            // refreshStaking();
            setStakingText('loading');
            const staking = await axios.get(`/staking/${localStorage.account}`);
            const res = staking.stakingAccounts !== 0;
            localStorage.isStaking = res;

            stakingText.text(ret);
          }, () => {
            message.warning('Invalid mnemonic');
          });
        }}
      />
    </div>
  );
};

export default Staking;
