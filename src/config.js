const explorer = localStorage.explorer ? JSON.parse(localStorage.explorer) : {
  block: 'https://blnscan.io/',
  blockHeight: 'https://blnscan.io/',
  tx: 'https://blnscan.io/',
  account: 'https://blnscan.io/',
};

export default {
  explorer,
  GENESIS_TIME: 1545555600,
  DEFAULT_CONFIRMATIONS: 10,
};
