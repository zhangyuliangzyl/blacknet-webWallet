import React from 'react';
import { Table } from 'antd';
import axios from 'axios';
import MnemonicModal from '_components/MnemonicModal';
import config from '@/config';
import { FormattedMessage } from '@/utils/locale';
import { getFormatBalance, releaseConfirm, message } from '@/utils/helper';

const { Column } = Table;

export default class CancelLease extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noData: false,
      loading: true,
      mnemonicModalVisible: false,
      mnemonicModalOption: {},

    };
    this.account = localStorage.account;
  }

  componentDidMount() {
    this.getRelease();
  }

  getRelease = async () => {
    const outLeases = await axios.get(`/wallet/${this.account}/outleases`);
    this.setState({ loading: false, noData: outLeases.length === 0, outLeases });
  }

  onCancle = (to, amount, height) => {
    // todo zyl
    this.setState({
      mnemonicModalVisible: true,
      mnemonicModalOption: { to, amount, height },
    });
  }

  render() {
    const {
      loading, noData, mnemonicModalVisible, mnemonicModalOption, outLeases,
    } = this.state;
    return (
      <div className="cancel_lease">
        <h3><FormattedMessage id="Out Leases" /></h3>
        <Table
          dataSource={outLeases}
          footer={() => noData ? (
            <div className="tx-foot">
              <div className="no_tx_yet">
                <div className="text-center">
                  <p><span><FormattedMessage id="No transactions yet" /></span></p>
                </div>
              </div>
            </div>
          ) : null}
          loading={loading}
        >
          <Column
            title={<FormattedMessage id="Index" />}
            align="center"
            key="index"
            render={(text, record, index) => index + 1}
          />
          <Column
            title={<FormattedMessage id="Account" />}
            align="center"
            key="account"
            dataIndex="publicKey"
            render={(text) => <a target="_blank" href={`${config.explorer.account}${text}`}>{text}</a>}
          />
          <Column
            title={<FormattedMessage id="Height" />}
            align="center"
            key="height"
            dataIndex="height"
          />
          <Column
            title={<FormattedMessage id="Amount" />}
            align="center"
            key="amount"
            render={(text) => getFormatBalance(text)}
          />
          <Column
            title={<FormattedMessage id="Cancel Lease" />}
            align="center"
            key="btn"
            render={(text, record) => {
              const { publicKey, amount, height } = record;
              return (
                <a href="#" className="cancel_lease_btn" onClick={() => this.onCancle(publicKey, amount, height)}>
                  Cancel
                </a>
              );
            }}
          />
        </Table>
        <MnemonicModal
          visible={mnemonicModalVisible}
          option={mnemonicModalOption}
          onCancel={() => this.setState({ mnemonicModalVisible: false })}
          onVerifySuccess={(mnemonic) => releaseConfirm(mnemonic, 'cancellease', mnemonicModalOption.amount, mnemonicModalOption.to, mnemonicModalOption.height, (data) => {
            message.success('Cancel Lease Success');

            // no such dom below
            // $('#cancel_lease_result').text(data).parent().removeClass("hidden")

            this.setState({ mnemonicModalVisible: false });
            this.getRelease();
          })}
        />
      </div>
    );
  }
}
