import React from 'react';
import axios from 'axios';
import { inject, observer } from 'mobx-react';
import { Table, notification } from 'antd';
import Column from 'antd/lib/table/Column';
import { FormattedMessage } from '@/utils/locale';
import config from '@/config';
import { getTxTypeName, getFormatBalance, unix_to_local_time } from '@/utils/helper';
import websocket from '@/utils/websocket';
import './index.less';

const { GENESIS_TIME } = config;

function serializeTx(transactions) {
  const txs = [];
  Object.keys(transactions).forEach((hash) => {
    const tx = transactions[hash];
    tx.hash = hash;
    txs.push(tx);
  });

  txs.sort((x, y) => y.time - x.time);

  return txs;
}


function openNotification(tx) {
  const dataType = tx.data[0].type;
  const txData = tx.data[0].data;
  const time = unix_to_local_time(tx.time);
  const type = getTxTypeName(dataType);
  let amount = getFormatBalance(txData.amount);

  if (type === 'Generated') {
    amount = getFormatBalance(tx.fee);
  }

  const args = {
    message: 'Incoming Transaction',
    description: (
      <>
        <p>Date: {time}</p>
        <p>Amount: {amount}</p>
        <p>Type: {type}</p>
      </>
    ),
  };
  notification.open(args);
}

@inject('blacknetStore')
@observer
export default class Transactions extends React.Component {
  constructor(props) {
    super(props);
    this.account = localStorage.account;
    this.state = {
      type: 'all',
      loading: false,
      noData: false,
      paginationProps: {
        pageSize: 100,
        defaultCurrent: 1,
        onChange: this.onPageChange,
        // position: 'both',
      },
    };
    this.txdb = {};
    this.allTransaction = [];
    this.filterTransaction = [];
    this.paginationalDb = {};
    this.renderKeyList = [];
    this.renderList = [];


    this.tabs = [
      { type: 'all', component: <FormattedMessage id="All" /> },
      { type: 0, component: <FormattedMessage id="Transfer" /> },
      { type: 'genesis', component: <FormattedMessage id="genesis">Genesis</FormattedMessage> },
      { type: 2, component: <FormattedMessage id="Lease" /> },
      { type: 3, component: <FormattedMessage id="Cancel Lease" /> },
      { type: 254, component: <FormattedMessage id="PoS Generated" /> },
    ];
  }

  componentDidMount() {
    const { blacknetStore: { onInitResolve } } = this.props;
    onInitResolve(this.initRecentTransactions);
    websocket.pushDataResolver((response) => {
      if (response.route === 'transaction') {
        const tx = response.message;
        const transactionKey = tx.height + tx.time;
        this.txdb[transactionKey] = tx;

        if (this.renderKeyList.includes(transactionKey)) {
          this.renderList.forEach((item, index, arr) => {
            if (transactionKey === item.height + item.time) {
              arr[index] = tx;
              this.setState(() => ({ renderTransaction: arr.slice(0) }));
            }
          });
        }

        if (tx.time * 1000 > Date.now() - 1000 * 60) {
          openNotification(tx);
        }
      }
    }, 'transaction');
  }

  initRecentTransactions = async () => {
    this.setState({ loading: true });
    const transactions = await axios.get(`/wallet/${this.account}/transactions`);
    this.allTransaction = serializeTx(transactions);

    const { paginationProps: { pageSize } } = this.state;
    const renderTransaction = this.allTransaction.slice(0, pageSize);
    await this.renderTable(renderTransaction);
  }

  getStatusText = async (height, hash) => {
    const { blacknetStore: { ledger } } = this.props;

    let confirmations = ledger.height - height + 1;
    let statusText = 'Confirmed';
    if (height === 0) {
      confirmations = await axios.get(`/wallet/${this.account}/confirmations/${hash}`);
      statusText = `${confirmations} Confirmations`;
    } else if (confirmations < config.DEFAULT_CONFIRMATIONS) {
      statusText = `${confirmations} Confirmations`;
    }
    return statusText;
  };

  renderTable = async (renderTransaction) => {
    let noData;
    this.renderList = renderTransaction;

    for (let i = 0; i < renderTransaction.length; i++) {
      const transaction = renderTransaction[i];
      const transactionKey = transaction.height + transaction.time;
      transaction.tx = this.txdb[transactionKey];
      this.renderKeyList.push(transactionKey);

      if (!transaction.tx) {
        transaction.tx = await axios.get(`/wallet/${this.account}/transaction/${transaction.hash}/false`);
        this.txdb[transactionKey] = transaction.tx;
      }

      transaction.tx.time = transaction.time;

      if (transaction.time * 1000 < Date.now() - 1000 * 10) {
        transaction.status = 'Confirmed';
      } else {
        transaction.status = await this.getStatusText(transaction.height, transaction.hash);
      }
    }

    const { paginationProps, type } = this.state;
    this.paginationalDb[paginationProps.current] = renderTransaction;
    const transactionList = type === 'all' ? this.allTransaction : this.filterTransaction;

    this.setState({
      loading: false,
      noData,
      renderTransaction,
      paginationProps: { ...paginationProps, total: transactionList.length },
    });
  }

  onPageChange = async (page, pageSize) => {
    const { paginationProps } = this.state;
    this.setState({
      loading: true,
      paginationProps: { ...paginationProps, current: page },
    });

    if (this.paginationalDb[page]) {
      this.setState({
        loading: false,
        renderTransaction: this.paginationalDb[page],
      });
    } else {
      const arr = this.state.type === 'all' ? this.allTransaction : this.filterTransaction;
      const renderTransaction = arr.slice((page - 1) * pageSize, page * pageSize);
      await this.renderTable(renderTransaction);
    }
  }

  onTabClick = async (type) => {
    const { paginationProps } = this.state;
    if (type === 'genesis') {
      this.filterTransaction = this.allTransaction.filter((tx) => tx.time === GENESIS_TIME);
    } else {
      this.filterTransaction = this.allTransaction.filter((tx) => tx.types[0].type === type && tx.height > 0);
    }

    const arr = type === 'all' ? this.allTransaction : this.filterTransaction;
    const renderTransaction = arr.slice(0, paginationProps.pageSize);

    this.setState({
      type, loading: true, paginationProps: { ...paginationProps, current: 1 },
    }, () => {
      this.paginationalDb = {};
      this.renderTable(renderTransaction);
    });
  }

  render() {
    const {
      loading, noData, renderTransaction, paginationProps, type,
    } = this.state;
    return (
      <div className="transactions">
        <h3><FormattedMessage id="recent transactions" /></h3>
        <div className="filter">
          {
            this.tabs.map((t) => (
              <a
                href="#"
                className={type === t.type ? 'active' : ''}
                onClick={() => {
                  this.onTabClick(t.type);
                }}
                key={t.type}
              >
                {t.component}
              </a>
            ))
          }

        </div>
        <Table
          dataSource={renderTransaction}
          pagination={paginationProps}
          rowKey="hash"
          footer={() => noData ? (
            <div className="tx-foot">
              <div className="no_tx_yet">
                <div className="text-center">
                  <p><span><FormattedMessage id="No transactions yet" /></span></p>
                </div>
              </div>
            </div>
          ) : null}
          loading={loading}
        >
          <Column
            title={<FormattedMessage id="time" />}
            key="time"
            dataIndex="time"
            render={(text) => unix_to_local_time(text)}
          />
          <Column
            title={<FormattedMessage id="type">Type</FormattedMessage>}
            key="type"
            render={(value, record) => {
              const { tx } = record;
              tx.height = record.height;
              tx.time = record.time;
              const dataType = tx.data[0].type;
              const type = getTxTypeName(dataType, tx, this.account);

              if (dataType === 0) {
                const text = this.account === tx.from ? 'Sent to' : 'Received from';
                return <a target="_blank" href={`${config.explorer.tx}${tx.hash}`}>{text}</a>;
              }

              if (dataType === 2 || dataType === 3) {
                return <a target="_blank" href={`${config.explorer.tx}${tx.hash}`}>{`${type} ${this.account === tx.from ? 'to' : 'from'}`}</a>;
              }

              if (dataType !== 254 && dataType !== 0 && dataType !== 2 && dataType !== 3) {
                return <a target="_blank" href={`${config.explorer.tx}${tx.hash}`}>{type}</a>;
              }
              return type;
            }}
          />
          <Column
            title={<FormattedMessage id="account" />}
            key="account"
            render={(value, record) => {
              const { tx } = record;
              const dataType = tx.data[0].type;
              const txData = tx.data[0].data;

              if (dataType === 0 || dataType === 2 || dataType === 3) {
                if (this.account === tx.from) {
                  return <a target="_blank" href={`${config.explorer.accoun}${txData.to}`}>{txData.to}</a>;
                }
                return <a target="_blank" href={`${config.explorer.accoun}${tx.from}`}>{tx.from}</a>;
              }

              if (dataType !== 0 && dataType !== 2 && dataType !== 3) {
                if (tx.from !== 'genesis') {
                  return <a target="_blank" href={`${config.explorer.account}${tx.from}`}>{tx.from}</a>;
                }
                return <a target="_blank" href={`${config.explorer.account}${tx.to}`}>{tx.to}</a>;
              }
              return '';
            }}
          />
          <Column
            title={<FormattedMessage id="amount" />}
            key="amount"
            render={(value, record) => {
              const { tx } = record;
              const txData = tx.data[0].data;
              const dataType = tx.data[0].type;

              let { amount } = txData;
              if (dataType === 254) {
                amount = tx.fee;
              }
              amount = getFormatBalance(amount);
              return <span className="strong">{amount}</span>;
            }}
          />
          <Column
            title=""
            key="status"
            dataIndex="status"
          />
        </Table>

        {/* {
          loading ? (
            <div className="spinner">
              <div className="rect1" />
              <div className="rect2" />
              <div className="rect3" />
              <div className="rect4" />
              <div className="rect5" />
            </div>
          ) : null
        } */}
      </div>
    );
  }
}
