import React from 'react';
import { Input, Form, Button } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import {
  verifyAccount, verifyMnemonic, postPromise, showProgress,
} from '@/utils/helper';
import history from '@/utils/history';
import './index.less';

const { account } = localStorage;

async function mnemonicToAddress(mnemonic) {
  const postdata = {
    mnemonic,
  };
  const { address } = await postPromise('/mnemonic', postdata, true);
  return address;
}

const PassedDom = () => (
  <div className="container">
    <div className="progress-stats">
      <div className="progress">
        <div className="progress-bar" role="progressbar" />
      </div>
    </div>
    <div className="progress-stats-text" />
  </div>
);

class AccountPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSpinner: true,
      showAccountForm: false,
      showNewAccount: false,
      warningChecked: false,
      warningInRed: false,
    };
  }


  componentDidMount() {
    setTimeout(() => {
      if (account) {
        showProgress();
        history.push('/overview');
      } else {
        this.setState({
          showSpinner: false,
          showAccountForm: true,
        });
      }
    }, 1000);
  }

  checkAccount = async (rule, value, callback) => {
    let accountInput = value;

    if (verifyAccount(accountInput)) {
      localStorage.account = accountInput;
    } else if (verifyMnemonic(accountInput)) {
      accountInput = await mnemonicToAddress(accountInput);
      localStorage.account = accountInput;
    } else {
      return callback('Invalid account/mnemonic');
    }
    return callback();
  }

  onSubmitAccount = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        window.location.reload();
      }
    });
  };

  onNewAccount = () => {
    const mnemonic = window.blacknetjs.Mnemonic();
    const address = window.blacknetjs.Address(mnemonic);

    this.setState({
      showAccountForm: false,
      showNewAccount: true,
    }, () => {
      document.getElementById('new_account_text').value = address;
      document.getElementById('new_mnemonic').value = mnemonic;
      window.isGenerated = true;
    });
  }

  onNewAccountNext = () => {
    if (!this.state.warningChecked) {
      this.setState({ warningInRed: true });
    } else {
      window.location.reload();
    }
    return false;
  }

  render() {
    const {
      showSpinner, showAccountForm, showNewAccount, warningChecked, warningInRed,
    } = this.state;
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="account-container">
        <div className="loading-container">
          <div className="body">
            {
              showNewAccount ? '' : (
                <>
                  <img src="images/logo.png" height="160" width="160" alt="" />
                  <h1>Blacknet Wallet</h1>
                </>
              )
            }
            {
              showSpinner ? (
                <div className="spinner">
                  <div className="rect1" />
                  <div className="rect2" />
                  <div className="rect3" />
                  <div className="rect4" />
                  <div className="rect5" />
                </div>
              ) : ''
            }
            {
              showAccountForm ? (
                <>
                  <Form onSubmit={this.onSubmitAccount}>
                    <Form.Item>
                      {getFieldDecorator('account_text', {
                        rules: [{ validator: this.checkAccount }, { transform: (value) => value ? value.trim() : value }],
                        validateTrigger: 'onSubmit',
                      })(
                        <Input.Password visibilityToggle={false} size="large" style={{ width: '502px', textAlign: 'center' }} placeholder="Your account or mnemonic..." autoComplete="off" />,
                      )}
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" htmlType="submit" className="v2">
                        <FormattedMessage id="enter" />
                      </Button>
                    </Form.Item>
                  </Form>
                  <a href="#" id="new_account" onClick={this.onNewAccount}><FormattedMessage id="create account" /></a>
                </>
              ) : ''
            }
          </div>
        </div>
        <div className="new-account-container" style={{ display: showNewAccount ? 'block' : 'none' }}>
          <div className="dialog newaccount" style={{ display: 'block' }}>
            <div className="body">
              <h1>Your automatically generated Mnemonic is:</h1>
              <div className="form">
                <div className="field-row">
                  <label><FormattedMessage id="mnemonic">Mnemonic</FormattedMessage></label>
                  <input type="text" id="new_mnemonic" readOnly />
                </div>
                <div className="field-row">
                  <label><FormattedMessage id="account">Account</FormattedMessage></label>
                  <input type="text" id="new_account_text" readOnly />
                </div>
              </div>
              <div id="confirm_mnemonic_warning_container">
                <input type="checkbox" id="confirm_mnemonic_warning" value={warningChecked} onChange={() => this.setState({ warningChecked: true })} />
                <span />
                <label htmlFor="confirm_mnemonic_warning" style={{ color: warningInRed ? 'red' : 'unset' }}>
                  <span>
                    {' '}
                    <FormattedMessage id="promise of save mnemonic" />
                  </span>
                </label>
              </div>
              <button id="new_account_next_step" className="ant-btn" onClick={this.onNewAccountNext}><FormattedMessage id="next">Next</FormattedMessage></button>
            </div>
          </div>
        </div>
        <PassedDom />
      </div>
    );
  }
}

export default Form.create()(AccountPage);
