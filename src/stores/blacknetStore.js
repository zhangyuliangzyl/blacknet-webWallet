import {
  action, observable, computed,
} from 'mobx';
import axios from 'axios';
import { renderProgressBar } from '@/utils/helper';

class BlacknetStore {
  @observable ledger = {};

  @observable network = {};

  @observable nodeInfo = { warnings: [] };

  isInit = false;

  constructor(rootStore) {
    this.rootStore = rootStore;
    this.loaded = false;
    this.loading = false;
    this.init();
  }

  @action
  init = () => {
    if (this.loaded || this.loading) {
      return;
    }

    Promise.all([this.getNetwork(), this.getNodeInfo()]).then((result) => {
      const [ledger, nodeInfo] = result;
      if (typeof this.initCallback === 'function') {
        this.initCallback({
          ledger,
          nodeInfo,
        });
      }

      this.hasCb = false;
      this.initCallback = undefined;
      this.isInit = true;

      this.getNetworkAction(ledger);
      this.getNodeInfoAction(nodeInfo);
    });
  }

  getNetwork = () => axios.get('/ledger');

  @action
  getNetworkAction = (ledger) => {
    this.loading = true;
    this.network = {
      supply: ledger.supply,
      height: ledger.height,
    };
    this.ledger = ledger;
    renderProgressBar(ledger.blockTime);
    this.loading = false;
    this.loaded = true;
  }

  getNodeInfo = () => axios.get('/node')

  @action
  getNodeInfoAction = async (nodeInfo) => {
    this.loading = true;
    this.loading = false;
    this.nodeInfo = nodeInfo;
  }

  onInitResolve = (cb) => {
    if (this.isInit) {
      cb({ ledger: this.ledger, network: this.network, nodeInfo: this.nodeInfo });
      return;
    }
    this.initCallback = cb;
    this.hasCb = true;
  }

  @computed get connections() {
    return this.nodeInfo.outgoing + this.nodeInfo.incoming;
  }
}

export default BlacknetStore;
