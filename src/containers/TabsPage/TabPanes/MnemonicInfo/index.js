import React from 'react';
import { Form, Input, Button } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import { checkMnemonic, postPromise } from '@/utils/helper';

class MnemonicInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showResult: false,
      result: '',
    };
  }

  onSubmitMnemonic = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { mnemonic_info_mnemonic: mnemonic } = values;
        const postdata = { mnemonic };

        postPromise('/mnemonic', postdata).then((data) => {
          this.setState({
            showResult: true,
            result: (
              <pre>
                {`Address: ${data.address}`} <br />
                {`Public Key: ${data.publicKey}`}
              </pre>
            ),
          });
        });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { showResult, result } = this.state;

    return (
      <div className="info">
        <div className="form">
          <legend><FormattedMessage id="mnemonic info" /></legend>
          <Form layout="inline">
            <div className="field-row">
              <Form.Item label={<FormattedMessage id="mnemonic" />}>
                {getFieldDecorator('mnemonic_info_mnemonic', {
                  rules: [{ validator: checkMnemonic }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: ['onSubmit', 'onBlur'],
                })(
                  <Input.Password size="large" style={{ width: '494px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" onClick={this.onSubmitMnemonic} style={{ marginRight: '0' }}>
                  <FormattedMessage id="mnemonic info" />
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="field-row field-row-outer" style={{ display: showResult ? 'block' : 'none' }}>
          <label><FormattedMessage id="result" /></label>
          {result}
        </div>
      </div>
    );
  }
}

export default Form.create()(MnemonicInfo);
